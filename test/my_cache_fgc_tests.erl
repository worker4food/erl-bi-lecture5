-module(my_cache_fgc_tests).

-include_lib("eunit/include/eunit.hrl").

alive_test() ->
    ?assertEqual(ok, my_cache_fgc:new(test_alive1)),
    {K, V} = {key, "Val"},
    ?assertEqual(ok,
        my_cache_fgc:insert(test_alive1, K, V, 10000)),
    V1 = my_cache_fgc:lookup(test_alive1, K),
    ?assertEqual({ok, V}, V1).

expired_test() ->
    ?assertEqual(ok, my_cache_fgc:new(test_exp1)),
    {K, V} = {key, "Val"},
    ?assertEqual(ok,
        my_cache_fgc:insert(test_exp1, K, V, -1)),
    V1 = my_cache_fgc:lookup(test_exp1, K),
    ?assertEqual(undefined, V1).
