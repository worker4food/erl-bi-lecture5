-module(my_cache_tests).

-include_lib("eunit/include/eunit.hrl").

alive_test() ->
    ?assertEqual(ok, my_cache:new(test_alive)),
    {K, V} = {key, "Val"},
    ?assertEqual(ok,
        my_cache:insert(test_alive, K, V, 10000)),
    V1 = my_cache:lookup(test_alive, K),
    ?assertEqual({ok, V}, V1).

expired_test() ->
    ?assertEqual(ok, my_cache:new(test_exp)),
    {K, V} = {key, "Val"},
    ?assertEqual(ok,
        my_cache:insert(test_exp, K, V, -1)),
    V1 = my_cache:lookup(test_exp, K),
    ?assertEqual(undefined, V1).
