-module(bench_update).

-export([
    map_syntax/1, bench_map_syntax/2,
    map_update/1, bench_map_update/2,
    proplist/1, bench_proplist/2,
    dict/1, bench_dict/2,
    proc_dict/1, bench_proc_dict/2,
    ets/1, bench_ets/2]).

-define(Sz, 1500).

input() ->
    Vals = lists:seq(1, ?Sz),
    Init = lists:zip(Vals, Vals),
    RL = [{rand:uniform(), X, ?Sz - X + 1} || X <- Vals],
    Rand = [{K, V} || {_, K, V} <- lists:sort(RL)],
    {Init, Rand}.

% Map update, built-in syntax
map_syntax({input, _}) ->
    {Init, KV} = input(),
    {KV, op_map_syntax:create(Init)}.

bench_map_syntax({Xs, Acc}, _) ->
    op_map_syntax:update(Xs, Acc).

% Map update, maps:update
map_update({input, _}) ->
    {Init, KV} = input(),
    {KV, op_map:create(Init)}.

bench_map_update({Xs, Acc}, _) ->
    op_map:update(Xs, Acc).

%Dict update
dict({input, _}) ->
    {Init, KV} = input(),
    {KV, op_dict:create(Init)}.

bench_dict({Xs, Dict}, _) ->
    op_dict:update(Xs, Dict).

% Proplist update
proplist({input, _}) ->
    {I, KV} = input(),
    {KV, I}. % gm..

bench_proplist({Xs, Acc}, _) ->
    op_proplist:update(Xs, Acc).

% Process dict update
proc_dict({stop, _}) ->
    erase();
proc_dict({input, _}) ->
    {Init, KV} = input(),
    Some = op_proc_dict:create(),
    {KV, op_proc_dict:insert(Init, Some)}.

bench_proc_dict({Xs, Some}, _) ->
    op_proc_dict:update(Xs, Some).

% ETS update
ets(init) ->
    op_ets:create();
ets({stop, Tab}) ->
    ets:delete(Tab);
ets({input, Tab}) ->
    {Init, KV} = input(),
    op_ets:insert(Init, Tab),
    KV.

bench_ets(Xs, Tab) ->
    op_ets:update(Xs, Tab).


