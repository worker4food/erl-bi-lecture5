-module(bench_lookup).

-export([
    map_syntax/1, bench_map_syntax/2,
    map_get/1, bench_map_get/2,
    proplist/1, bench_proplist/2,
    dict/1, bench_dict/2,
    proc_dict/1, bench_proc_dict/2,
    ets/1, bench_ets/2]).

-define(Sz, 1500).

input() ->
    Vals = lists:seq(1, ?Sz),
    Init = lists:zip(Vals, Vals),
    RL = [{rand:uniform(), X} || X <- Vals],
    Rand = [K || {_, K} <- lists:sort(RL)],
    {Init, Rand}.

% Map lookup, built-in syntax
map_syntax({input, _}) ->
    {Init, Ks} = input(),
    {Ks, op_map_syntax:create(Init)}.

bench_map_syntax({Xs, Acc}, _) ->
    op_map_syntax:lookup(Xs, Acc).

% Map lookup, maps:get
map_get({input, _}) ->
    {Init, Ks} = input(),
    {Ks, op_map:create(Init)}.

bench_map_get({Xs, Acc}, _) ->
    op_map:lookup(Xs, Acc).

%Dict lookup
dict({input, _}) ->
    {Init, Ks} = input(),
    {Ks, op_dict:create(Init)}.

bench_dict({Xs, Dict}, _) ->
    op_dict:lookup(Xs, Dict).

% Proplist lookup
proplist({input, _}) ->
    {I, Ks} = input(),
    {Ks, I}. % gm..

bench_proplist({Xs, Acc}, _) ->
    op_proplist:lookup(Xs, Acc).

% Process dict lookup
proc_dict({stop, _}) ->
    erase();
proc_dict({input, _}) ->
    {Init, Ks} = input(),
    Some = op_proc_dict:create(),
    {Ks, op_proc_dict:insert(Init, Some)}.

bench_proc_dict({Xs, Some}, _) ->
    op_proc_dict:lookup(Xs, Some).

% ETS lookup
ets(init) ->
    op_ets:create();
ets({stop, Tab}) ->
    ets:delete(Tab);
ets({input, Tab}) ->
    {Init, Ks} = input(),
    op_ets:insert(Init, Tab),
    Ks.

bench_ets(Xs, Tab) ->
    op_ets:lookup(Xs, Tab).


