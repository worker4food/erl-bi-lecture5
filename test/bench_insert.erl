-module(bench_insert).

-export([
    map_syntax/1, bench_map_syntax/2,
    map_put/1, bench_map_put/2,
    proplist/1, bench_proplist/2,
    dict/1, bench_dict/2,
    proc_dict/1, bench_proc_dict/2,
    ets/1, bench_ets/2]).

-define(Sz, 1500).

input() ->
    L = lists:seq(1, ?Sz),
    lists:zip(L, L).

% Map insert, built-in syntax
map_syntax({input, _}) ->
    {input(), op_map_syntax:create()}.

bench_map_syntax({Xs, Acc}, _) ->
    op_map_syntax:insert(Xs, Acc).

% Map insert, maps:put
map_put({input, _}) ->
    {input(), op_map:create()}.

bench_map_put({Xs, Acc}, _) ->
    op_map:insert(Xs, Acc).

%Dict insert
dict({input, _}) ->
    {input(), op_dict:create()}.

bench_dict({Xs, Dict}, _) ->
    op_dict:insert(Xs, Dict).

% Proplist insert
proplist({input, _}) ->
    {input(), []}.

bench_proplist({Xs, Acc}, _) ->
    op_proplist:insert(Xs, Acc).

% Process dict insert
proc_dict({stop, _}) ->
    erase();
proc_dict({input, _}) ->
    {input(), op_proc_dict:create()}.

bench_proc_dict({Xs, Some}, _) ->
    op_proc_dict:insert(Xs, Some).

% ETS insert
ets(init) ->
    op_ets:create();
ets({stop, Tab}) ->
    ets:delete(Tab);
ets({input, _}) ->
    input().

bench_ets(Xs, Tab) ->
    op_ets:insert(Xs, Tab).
