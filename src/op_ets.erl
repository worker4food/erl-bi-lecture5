-module(op_ets).

-export([create/0, insert/2, update/2, lookup/2]).

create() ->
    ets:new(noname, [set, public]).

insert([X | Xs], T) ->
    ets:insert_new(T, X),
    insert(Xs, T);
insert([], _) ->
    ok.

update([{K, V} | Xs], T) ->
    ets:update_element(T, K, {2, V}),
    update(Xs, T);
update([], _) ->
    ok.

lookup([K | Ks], T) ->
    [_] = ets:lookup(T, K),
    lookup(Ks, T);
lookup([], _) ->
    ok.
