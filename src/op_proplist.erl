-module(op_proplist).

-export([create/0, insert/2, update/2, lookup/2]).

create() ->
    [].

insert([KV | Rest], Acc) ->
    insert(Rest, [KV | Acc]);
insert([], Acc) ->
    Acc.

update([{K, V} | Rest], Acc) ->
    Acc1 = lists:keystore(K, 1, Acc, {K, V}),
    update(Rest, Acc1);
update([], Acc) ->
    Acc.

lookup([K | Ks], Ps) ->
    {_, _} = proplists:lookup(K, Ps),
    lookup(Ks, Ps);
lookup([], Ps) ->
    Ps.
