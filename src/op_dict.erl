-module(op_dict).

-export([create/0, create/1, insert/2, update/2, lookup/2]).

create() ->
    dict:new().

create(KV) ->
    dict:from_list(KV).

insert([{K, V} | Xs], Acc) ->
    Acc1 = dict:store(K, V, Acc),
    insert(Xs, Acc1);
insert([], Acc) ->
    Acc.

update([{K, V} | Xs], Acc) ->
    Acc1 = dict:update(K, fun (_) -> V end, Acc),
    update(Xs, Acc1);
update([], Acc) ->
    Acc.

lookup([K | Ks], D) ->
    {ok, _} = dict:find(K, D),
    lookup(Ks, D);
lookup([], D) ->
    D.
