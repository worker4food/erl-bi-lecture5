-module(op_map_syntax).

-export([create/0, create/1, insert/2, update/2, lookup/2]).

create() ->
    #{}.

create(KV) ->
    maps:from_list(KV).

insert([{K, V} | Rest], M) ->
    insert(Rest, M#{K => V});
insert([], M) ->
    M.

update([{K, V} | Rest], M) ->
    update(Rest, M#{K := V});
update([], M) ->
    M.

lookup([K | Ks], M) ->
    #{K := _} = M,
    lookup(Ks, M);
lookup([], M) ->
    M.
