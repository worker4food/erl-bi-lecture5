-module(op_proc_dict).

-export([create/0, insert/2, update/2, lookup/2]).

create() -> ok.

insert([{K, V} | Rest], ok) ->
    put(K, V),
    insert(Rest, ok);
insert([], ok) ->
    ok.

update(Xs, Some) ->
    insert(Xs, Some).

lookup([K | Ks], ok) ->
    _ = get(K),
    lookup(Ks, ok);
lookup([], ok) ->
    ok.
