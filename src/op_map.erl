-module(op_map).

-export([create/0, create/1, insert/2, update/2, lookup/2]).

create() ->
    maps:new().

create(KV) ->
    maps:from_list(KV).

insert([{K, V} | Rest], M) ->
    insert(Rest, maps:put(K, V, M));
insert([], M) ->
    M.

update([{K, V} | Rest], M) ->
    update(Rest, maps:update(K, V, M));
update([], M) ->
    M.

lookup([K | Ks], M) ->
    _ = maps:get(K, M),
    lookup(Ks, M);
lookup([], M) ->
    M.
