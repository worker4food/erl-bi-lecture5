% FGC - fast GC
-module(my_cache_fgc).

-export([new/1, insert/4, lookup/2, delete_obsolete/1]).

-record(tables, {data, index}).
-record(eol, {exp, salt = 0}). % End of life (eol) record, used as index

new(TableName) ->
    undefined = get(TableName), % fail, when cache <TableName> already exists
    I = #tables {
        data = ets:new(data, [set, public]),
        index = ets:new(index, [ordered_set, public])},
    put(TableName, I),
    ok.

insert(TableName, Key, Val, TimeSec) ->
    IxKey = #eol {
        exp = TimeSec + erlang:monotonic_time(second),
        salt = erlang:unique_integer([positive])},
    #tables {data = Data, index = Ix} = get(TableName),
    true = case ets:insert_new(Data, {Key, Val, IxKey}) of
        true -> true;
        false ->
            OldIxKey = ets:lookup_element(Data, Key, 3),
            ets:delete(Ix, OldIxKey),
            ets:update_element(Data, Key, [{2, Val}, {3, IxKey}])
    end,
    ets:insert(Ix, {IxKey, Key}),
    ok.

lookup(TableName, Key) ->
    #tables {data = Data} = get(TableName),
    Ts = erlang:monotonic_time(second),
    case ets:lookup(Data, Key) of
        [{_, Val, #eol {exp = Exp}}] when Exp >= Ts -> {ok, Val};
        _ -> undefined
    end.

delete_obsolete(TableName) ->
    T = get(TableName),
    StartK = ets:prev(
        T#tables.index,
        #eol {exp = erlang:monotonic_time(second)}),
    del_before(T, StartK).

del_before(_, '$end_of_table') ->
    ok;
del_before(#tables {data = Data, index = Ix} = T, Key) ->
    DataKey = ets:lookup_element(Ix, Key, 2),
    ets:delete(Data, DataKey),
    ets:delete(Ix, Key),
    del_before(T, ets:prev(Ix, Key)).
