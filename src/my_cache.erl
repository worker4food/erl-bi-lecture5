-module(my_cache).

-export([new/1, insert/4, lookup/2, delete_obsolete/1]).

-record(data, {key, val, eol}). % eol - end of life

new(TableName) ->
    undefined = get(TableName), % fail, when cache <TableName> already exists
    T = ets:new(data, [set, public, {keypos, #data.key}]),
    put(TableName, T),
    ok.

insert(TableName, Key, Val, TimeSec) ->
    Eol = TimeSec + erlang:monotonic_time(second),
    Data = get(TableName),
    ets:insert(Data, #data {key = Key, eol = Eol, val = Val}),
    ok.

lookup(TableName, Key) ->
    Data = get(TableName),
    Ts = erlang:monotonic_time(second),
    MHd = #data {key = Key, eol = '$1', val = '$2'},
    case ets:select(Data, [{MHd, [{'>=', '$1', Ts}], ['$2']}]) of
        [Val] -> {ok, Val};
        _ -> undefined
    end.

delete_obsolete(TableName) ->
    Data = get(TableName),
    Ts = erlang:monotonic_time(second),
    ets:select_delete(Data,
        [{#data {eol = '$1', _ = '_'}, [{'<', '$1', Ts}], [true]}]),
    ok.
