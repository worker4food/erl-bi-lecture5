## Usage

```$ rebar3 bench```

> Note: windows not supported!

## Results

> All benchmarks are performed on a dataset of 1500 elements

> All numbers are operations per millisecond

### insert
|bench     |min       |mean      |max       |
|      ---:|:---      |:---      |:---      |
|  proplist|148374    |177087    |189386    |
| proc_dict|38165     |41831     |46685     |
|       ets|13205     |14607     |15386     |
|map_syntax|5569      |6920      |8189      |
|   map_put|4161      |6960      |7587      |
|      dict|1338      |1547      |1667      |

### lookup
|bench     |min       |mean      |max       |
|      ---:|:---      |:---      |:---      |
| proc_dict|131484    |172313    |180292    |
|map_syntax|24229     |28527     |30469     |
|   map_get|22887     |27053     |29503     |
|       ets|10072     |11803     |12662     |
|      dict|5034      |5660      |6346      |
|  proplist|72        |85        |90        |

### update
|bench     |min       |mean      |max       |
|      ---:|:---      |:---      |:---      |
| proc_dict|37359     |40907     |43099     |
|       ets|9918      |11406     |11982     |
|map_update|5693      |6501      |6825      |
|map_syntax|5196      |5880      |6259      |
|      dict|1616      |1985      |2130      |
|  proplist|73        |85        |89        |
